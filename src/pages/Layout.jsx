import { Suspense, useEffect, useState } from "react";
import { Outlet, useLocation } from "react-router-dom";
import Header from "../components/Header";

export default function Layout() {
    let location = useLocation();
    const [nbChangement, setNbChangement] = useState(0)
    useEffect(() => {
        setNbChangement((oldValue) => oldValue + 1);
    }, [location])

    return <>
        <Header />
        {nbChangement}
        <Suspense fallback={<>...</>}>
            <Outlet />
        </Suspense>
        {/*<Footer />*/}
    </>
}