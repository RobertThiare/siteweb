import Compteur from '../components/Compteur';
import woodie from '../resources/woodie.jpg';

export default function Accueil() {
    return <div>
        <a href="#test">Test</a>
        <h2>Accueil</h2>
        Allo

        <Compteur />

        <img src={woodie} alt="Woodie, not impressed" />

        <p>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Reiciendis minus sit, molestiae atque quas veritatis? Rerum accusamus fugiat nobis a, eius odio excepturi. Laboriosam ducimus officia quam. Voluptatem, soluta labore.
        </p>
        <p>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Reiciendis minus sit, molestiae atque quas veritatis? Rerum accusamus fugiat nobis a, eius odio excepturi. Laboriosam ducimus officia quam. Voluptatem, soluta labore.
        </p>
        <p id="test">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Reiciendis minus sit, molestiae atque quas veritatis? Rerum accusamus fugiat nobis a, eius odio excepturi. Laboriosam ducimus officia quam. Voluptatem, soluta labore.
        </p>
    </div>
}