import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import ContentToggler from '../components/ContentToggler';
import Gallerie from '../components/Gallerie';
import Timer from '../components/Timer';

import tabPokemon from '../resources/gallerie-pokemon.json';
import notimpressed from '../resources/woodie.jpg';

//tabPokemon.push({ src: notimpressed, alt: 'Not impressed' });

export default function Projet1() {
    let navigate = useNavigate();
    const [redirection, setRedirection] = useState('/');
    
    const toAccueil = () => setRedirection('/');
    const toProjet1 = () => setRedirection('/projet1');
    const toProjet2 = () => setRedirection('/projet2');
    
    const go = () => {
        navigate(redirection);
    }

    return <div>
        <h2>Projet 1</h2>

        <ContentToggler title="Test content toggler" >
            <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque aspernatur minus assumenda voluptates culpa. Nesciunt magnam optio incidunt, sed, reprehenderit deleniti voluptas quia tenetur fugit odio delectus eligendi architecto? Facere.
            </p>
            <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Totam tempora cupiditate culpa molestiae quibusdam numquam delectus asperiores soluta repellat sunt, quam accusantium voluptatibus? Amet alias maxime animi modi reprehenderit reiciendis.
            </p>
        </ContentToggler>

        <Timer />

        <Gallerie images={tabPokemon} />

        <a href={notimpressed} download="Allo.jpg">Download</a>

        <button onClick={toAccueil}>Accueil</button>
        <button onClick={toProjet1}>Projet 1</button>
        <button onClick={toProjet2}>Projet 2</button>
        <button onClick={go}>Go!</button>
    </div>
}