import { NavLink } from 'react-router-dom';
import styles from './MenuNav.module.css';

export default function MenuNav() {
    return <nav>
        <ul className={styles.list}>
            <li>
                <NavLink to="/" className={({isActive}) => isActive ? styles.actif : ''}>
                    Accueil
                </NavLink>
            </li>
            <li>
                <NavLink to="/projet1" className={({isActive}) => isActive ? styles.actif : ''}>
                    Projet 1
                </NavLink>
            </li>
            <li>
                <NavLink to="/projet2" className={({isActive}) => isActive ? styles.actif : ''}>
                    Projet 2
                </NavLink>
            </li>
        </ul>
    </nav>
}