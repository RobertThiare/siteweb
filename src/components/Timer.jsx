import { useEffect, useState } from 'react';

import styles from './Timer.module.css';

export default function Timer() {
    const [nombre, setNombre] = useState(10);

    useEffect(() => {
        // Exécuté au montage
        console.log('Monter');

        // Interval qui est executé à chaque seconde
        let intervalId = setInterval(() => {
            console.log('Interval');

            // On modifie le nombre en fonction de sa dernière valeur
            setNombre((vieuxNombre) => {
                if (vieuxNombre > 0) {
                    // Si le nombre est plus grand que zéro, on décrémente
                    return vieuxNombre - 1;
                }
                else {
                    // Si le nombre est égal à zéro, on stop l'interval
                    clearInterval(intervalId);
                    return 0;
                }
            });
        }, 1000);

        // Exécuté au démontage
        return () => {
            clearInterval(intervalId);
        }
    }, []);

    return <div className={styles.timer}>
        {nombre}
    </div>
}