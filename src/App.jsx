import { Route, Routes } from 'react-router-dom';
import { lazy } from 'react';
import Layout from './pages/Layout';

import './App.css';

const Accueil = lazy(() => import('./pages/Accueil'));
const Projet1 = lazy(() => import('./pages/Projet1'));

export default function App() {
    return <>
        <Routes>
            <Route path="/" element={<Layout />}>
                <Route index element={<Accueil />} />
                <Route path="projet1" element={<Projet1 />} />
                <Route path="projet2" element={
                    <div>
                        Allo Page test temporaire
                    </div>
                } />
            </Route>
        </Routes>
    </>
}
